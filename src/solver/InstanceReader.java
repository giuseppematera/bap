package solver;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

public class InstanceReader {
	
	private ArrayList <ArrayList <Integer>> handlingTime;	
	//time a ship takes to be processed; 2 dimensional matrix: rows are berths, columns are ships; 0 means not possible
	private ArrayList <ArrayList <Integer>> TimeWindow;		
	//Berth time window. 3 values: start and end of availability; length of the tw 
	private ArrayList <Integer> ArrivalTime;				
	//When a ship arrives; unit:hours; time horizon: one week.
	
	private Scanner in;
	private int numShip;
	private int numBeth;
	
	public InstanceReader(String filename)  {
		File data = new File(filename);
		try {
			in=new Scanner(data);
			numShip= in.nextInt();
			numBeth= in.nextInt();
			
			handlingTime=handlingTimeReader(); 
			TimeWindow=TimeWindowReader();
			ArrivalTime=ArrivalTimereader();
			in.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
	
	
	
	public ArrayList <ArrayList <Integer>> getHandlingTime(){
		return handlingTime;
	}
	
	public ArrayList <ArrayList <Integer>> getTimeWindow(){
		return TimeWindow;
	}
	
	public ArrayList <Integer> getArrivalTime(){
		return ArrivalTime;
	}
	
	
	protected ArrayList <ArrayList <Integer>> handlingTimeReader(){
		
		ArrayList <ArrayList <Integer>> handlingTimeList = new ArrayList <ArrayList <Integer>>();
		
		for (int i=0;i<=numBeth-1;i++) {
			handlingTimeList.add(new ArrayList <Integer>());
			for (int j=0;j<=numShip-1;j++) handlingTimeList.get(i).add(in.nextInt());
		}
		
		return handlingTimeList;
	}
	
	protected ArrayList <ArrayList <Integer>> TimeWindowReader(){
		
		ArrayList <ArrayList <Integer>> TimeWindowList = new ArrayList <ArrayList <Integer>>();
		
		for (int i=0;i<=numBeth-1;i++) {
			TimeWindowList.add(new ArrayList <Integer>());	
			for (int j=0;j<=2;j++) TimeWindowList.get(i).add(in.nextInt());
		}		
		return TimeWindowList;
	}
	
	protected ArrayList <Integer> ArrivalTimereader(){
		
		ArrayList <Integer> ArrivalTimeList=new ArrayList <Integer>();
		
		for (int j=0;j<=numShip-1;j++) {
			ArrivalTimeList.add(in.nextInt());	
		}
		for (int j=0;j<=numShip-1;j++) ArrivalTimeList.add(in.nextInt());
		return ArrivalTimeList ;
	}



	public Scanner getIn() {
		return in;
	}



	public void setIn(Scanner in) {
		this.in = in;
	}



	public int getNumShip() {
		return numShip;
	}



	public void setNumShip(int numShip) {
		this.numShip = numShip;
	}



	public int getNumBeth() {
		return numBeth;
	}



	public void setNumBeth(int numBeth) {
		this.numBeth = numBeth;
	}



	public void setHandlingTime(ArrayList<ArrayList<Integer>> handlingTime) {
		this.handlingTime = handlingTime;
	}



	public void setTimeWindow(ArrayList<ArrayList<Integer>> timeWindow) {
		TimeWindow = timeWindow;
	}



	public void setArrivalTime(ArrayList<Integer> arrivalTime) {
		ArrivalTime = arrivalTime;
	}
	
}
