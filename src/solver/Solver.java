package solver;
import java.util.ArrayList;
import java.util.Collections;

import comparator.ShipComparator;
import model.Berth;
import model.Ship;

public class Solver {
	
	private ArrayList<Ship> ships;		
	private ArrayList<Berth> berths;
	private InstanceReader ir;
	private final int MAX_END_TW=300;  //time horizon
	
	//reading the file and creating the lists of berths and ships
	public Solver(String filename){
		ir=new InstanceReader(filename);
		berths=new ArrayList<>();
		ships=new ArrayList<>();
		readBerths();
		readShips();
		
	}
	
	//creating the list of berths
	protected void readBerths(){
		for (int i=0;i<ir.getNumBeth();i++){
			ArrayList<Integer> handlingTime=ir.getHandlingTime().get(i);
			ArrayList<Integer> timeWindow=ir.getTimeWindow().get(i);
			Berth b=new Berth(timeWindow.get(0),timeWindow.get(1),timeWindow.get(2),handlingTime,i);
			berths.add(b);
		}
	}
	
	//creating the list of ships
	protected void readShips(){
		for (int i=0;i<ir.getNumShip();i++){
			Ship s=new Ship(i,ir.getArrivalTime().get(i));
			ships.add(s);
			Collections.sort(ships,new ShipComparator());
		}
	}
	
	//to get a list containing only the ships that are already at the port 
	protected ArrayList<Ship> getShipsAtPort(){
		ArrayList<Ship> shipsAtPort=new ArrayList<>();
		for (Ship s:ships){
			if(s.isArrivedAtPort()) shipsAtPort.add(s);
		}
		return shipsAtPort;
	}
	
	//among the berth available and that can process the ship within the time window take the one that has the minimum handling time for the that ship
	protected Berth getBestBerth(Ship s,int hour){
		Berth bestBerth=null;
		int minHt=99999;
		for(Berth b:berths){
			if(b.isAvailable()) {
				int ht=b.getHandlingTime().get(s.getId());
				if(ht<minHt & (ht+hour)<b.getEndTW()){
					minHt=ht;
					bestBerth=b;
				}
			}
		}
		return bestBerth;		
	}
	
	
	public void solveHeuristic(){
		
		for(int hour=0;hour<MAX_END_TW;hour++){ 					//we decide to consider hour by hour 
			
			for(Ship s:ships){										//set as arrived all the ship that arrive at the port
				if(s.getArrivalTime()==hour) s.setArrivedAtPort(true);
				if(s.getArrivalTime()>hour) break;
			}	
			
			for (Berth b:berths){									//set as available all the berth that:
				if(b.getStartTW()==hour) b.setAvailable(true);		//are open
				if(b.getBusyUntil()==hour) b.setAvailable(true);	//or are done with the previous job
				if(b.getEndTW()==hour) b.setAvailable(false);		//or set as not available the ones that are closed
			}
			
			for (Ship s:getShipsAtPort()){							//for all the ship that are arrived at the port 
				Berth b=getBestBerth(s,hour);						//take the berth that has the fastest handling time among the available ones 
				if(b!=null){										//if any berth is available skip and wait until next hour
																	//if a berth is found 
					s.setBerth(b);									//assign the berth to that ship
					s.setDone(true);								//set the ship as done
					s.setArrivedAtPort(false);						//set the ship as that is not at the port anymore
					int ht=b.getHandlingTime().get(s.getId());		//handling time  
					b.setBusyUntil(hour+ht+1);						//set the berth busy until it is done 
					b.setAvailable(false);							//set the berth as not available		
				}
			}
		}		
	}

	
	public ArrayList<Ship> getShips() {
		return ships;
	}

	public void setShips(ArrayList<Ship> ships) {
		this.ships = ships;
	}

	public ArrayList<Berth> getBerths() {
		return berths;
	}

	public void setBerths(ArrayList<Berth> berths) {
		this.berths = berths;
	}

	public InstanceReader getIr() {
		return ir;
	}

	public void setIr(InstanceReader ir) {
		this.ir = ir;
	}

	public int getMAX_END_TW() {
		return MAX_END_TW;
	}
	

}
