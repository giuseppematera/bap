package comparator;
import java.util.Comparator;

import model.Ship;

public class ShipComparator implements Comparator<Ship> {
	
	public ShipComparator(){}

	//ships are compared by the arrival time
	@Override
	public int compare(Ship s0, Ship s1) {
		// TODO Auto-generated method stub
		int a0=s0.getArrivalTime();
		int a1=s1.getArrivalTime();
		
		if(a0<a1) return -1;
		
		else if(a0==a1) return 0;
		
		else return 1;

	}

}
