package model;
import java.util.ArrayList;

public class Berth {

	private int id;								//unique id 
	private int StartTW;						//opening time 
	private int EndTW;							//closing time
	private int LengthTW;						//length of the time window
	private ArrayList<Integer> HandlingTime;	//process time depending on the berth and the ship
	private boolean available;					//is the berth available or busy?
	private int busyUntil;						// if busy when it is gonna be available again
	
	public Berth(int StartTW,int EndTW,int LengthTW,ArrayList<Integer> HandlingTime,int id){
		this.id=id;
		this.StartTW=StartTW;
		this.EndTW=EndTW;
		this.LengthTW=LengthTW;
		this.HandlingTime=HandlingTime;
		this.available=false;
		this.busyUntil=-1;
	}
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getStartTW() {
		return StartTW;
	}
	public void setStartTW(int startTW) {
		StartTW = startTW;
	}
	public int getEndTW() {
		return EndTW;
	}
	public void setEndTW(int endTW) {
		EndTW = endTW;
	}
	public ArrayList<Integer> getHandlingTime() {
		return HandlingTime;
	}
	public void setHandlingTime(ArrayList<Integer> handlingTime) {
		HandlingTime = handlingTime;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public int getBusyUntil() {
		return busyUntil;
	}
	public void setBusyUntil(int busyUntil) {
		this.busyUntil = busyUntil;
	}

	public int getLengthTW() {
		return LengthTW;
	}

	public void setLengthTW(int lengthTW) {
		LengthTW = lengthTW;
	}
	
}
