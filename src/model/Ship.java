package model;


public class Ship {
	
	private int id;						//unique id 
	private int arrivalTime;			//when the ship arrives at the port
	private int serviceTime;			//when the ship moor  
	private boolean arrivedAtPort;		//is the ship at the port?
	private boolean done;				//is the ship processed
	private Berth berth;				//the berth in which the ship is processed
	
	
	public Ship(int id,int arrivalTime){
		this.id=id;
		this.arrivalTime=arrivalTime;
		this.arrivedAtPort=false;
		this.done=false;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getArrivalTime() {
		return arrivalTime;
	}
	public void setArrivalTime(int arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
	public boolean isArrivedAtPort() {
		return arrivedAtPort;
	}
	public void setArrivedAtPort(boolean arrivedAtPort) {
		this.arrivedAtPort = arrivedAtPort;
	}
	public boolean isDone() {
		return done;
	}
	public void setDone(boolean done) {
		this.done = done;
	}
	public Berth getBerth() {
		return berth;
	}
	public void setBerth(Berth berth) {
		this.berth = berth;
	}
	
	public String toString(){
		return "id: " +id+" arrives at: "+arrivalTime;
	}
	
	@Override
	public boolean equals(Object o){
		Ship s=(Ship)o;
		return this.arrivalTime==s.getArrivalTime();
	}

	public int getServiceTime() {
		return serviceTime;
	}

	public void setServiceTime(int serviceTime) {
		this.serviceTime = serviceTime;
	}
}
